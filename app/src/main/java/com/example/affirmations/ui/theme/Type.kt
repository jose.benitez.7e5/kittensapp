/*
 * Copyright (C) 2021 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.affirmations.ui.theme

import androidx.compose.material.Typography
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.sp

// Set of Material typography styles to start with
val Typography = Typography(
    body1 = TextStyle(
        fontFamily = FontFamily.Default,
        fontWeight = FontWeight.Normal,
        fontSize = 16.sp
    )
)
/*
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            AffirmationApp()
            // TODO 5. Show screen
        }
    }
}

@Composable
fun AffirmationApp() {
    // TODO 4. Apply Theme and affirmation list
    val card: Datasource = Datasource()
    AffirmationsTheme(darkTheme = true) {
        AffirmationList(affirmationList = card.loadAffirmations())
    }

}

@Composable
fun AffirmationList(affirmationList: List<Affirmation>, modifier: Modifier = Modifier) {
    // TODO 3. Wrap affirmation card in a lazy column
    LazyColumn() {
        this.items(
            items = affirmationList,
            itemContent = { item -> AffirmationCard(affirmation = item) })
    }
}

@Composable
fun AffirmationCard(affirmation: Affirmation, modifier: Modifier = Modifier) {
    // TODO 1. Your card UI
    val expanded = remember { mutableStateOf(false) }
    Card(
        modifier = Modifier
            .fillMaxSize()
            .padding(vertical = 5.dp)
    ) {
        Column(modifier = Modifier
       .animateContentSize(
           animationSpec = spring(
               dampingRatio = Spring.DampingRatioMediumBouncy,
               stiffness = Spring.StiffnessLow
           )
       )) {
            Row() {
                Image(
                    painter = painterResource(id = affirmation.imageResourceId),
                    contentDescription = "image",
                    modifier = Modifier
                        .padding(8.dp)
                        .size(64.dp)
                        .clip(RoundedCornerShape(50)),
                    contentScale = ContentScale.Crop
                )
                Text(
                    text = stringResource(id = affirmation.stringResourceId),
                    textAlign = TextAlign.Center,
                    fontSize = 20.sp,
                    modifier = Modifier
                        .padding(top = 13.dp, end = 2.dp)
                        .weight(1f)
                )
                AffirmationCardButton(
                    expanded = expanded.value,
                    onClick = {
                        expanded.value = !expanded.value
                    }
                )
            }
            if (expanded.value) {
                Description()
            }
        }
    }
}

@Composable
fun AffirmationCardButton(
    expanded: Boolean,
    onClick: () -> Unit,
    modifier: Modifier = Modifier
) {
    IconButton(onClick = onClick) {
        Icon(
            imageVector = Icons.Filled.ExpandMore,
            tint = MaterialTheme.colors.secondary,
            contentDescription = "hola", modifier = Modifier.size(60.dp)
        )
    }

}

@Composable
fun Description(modifier: Modifier = Modifier) {
    Column(
        modifier = modifier.padding(
            start = 16.dp,
            top = 8.dp,
            bottom = 16.dp,
            end = 16.dp
        )
    ) {
        Text(
            text = AnnotatedString("Description: "),
            style = MaterialTheme.typography.h3,
        )
        Text(
            text = AnnotatedString("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec lobortis, justo commodo ultrices fringilla, augue purus lobortis ex, sed ullamcorper nibh quam vitae velit. Ut tincidunt metus nec orci cursus maximus. Nam ornare massa at fringilla commodo. Etiam vestibulum sem nec aliquet pulvinar. "),
            style = MaterialTheme.typography.body1,
        )
    }
}
   /* @Preview
    @Composable
    private fun AffirmationCardPreview() {
        // TODO 2. Preview your card
        var card = Datasource()
        AffirmationCard(affirmation = card.loadAffirmations()[0])
    }*/
 */