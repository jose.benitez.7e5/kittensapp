package com.example.affirmations.ui.ViewModel

import androidx.lifecycle.ViewModel
import com.example.affirmations.data.Datasource
import com.example.affirmations.model.Affirmation
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow

class DetailViewModel:ViewModel() {
    private val _uistate= MutableStateFlow<Affirmation?>(null)
    val uiState: StateFlow<Affirmation?> = _uistate.asStateFlow()

    fun init(id: Int){
        _uistate.value = Datasource().loadAffirmations().firstOrNull{ affirmation -> affirmation.affirmationID == id}
    }
}